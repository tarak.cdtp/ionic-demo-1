import {Component, OnInit} from '@angular/core';
import {RecetteService} from '../service/recette.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  recettekey = '';

  constructor(public recetteService: RecetteService) { }

  ngOnInit() {

  }

}
