import {Ingredient} from './ingredient';

export interface Recette {
  id: number;
  nom: string;
  image: string;
  decription: string;
  ingredients: Ingredient[];
}
