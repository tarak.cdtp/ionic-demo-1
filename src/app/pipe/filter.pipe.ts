import { Pipe, PipeTransform } from '@angular/core';
import {Recette} from '../model/recette';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: Recette[], input: string): unknown {
    console.log(value);
    if (input) {
      return value.filter(val => val.nom.toLowerCase().includes(input.toLowerCase()));
  } else {
      return value;
    }
  }

}
