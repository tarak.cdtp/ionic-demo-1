import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RecetteService} from '../service/recette.service';
import {Recette} from '../model/recette';

@Component({
  selector: 'app-recette',
  templateUrl: './recette.page.html',
  styleUrls: ['./recette.page.scss'],
})
export class RecettePage implements OnInit {

  recette: Recette;
  constructor(private route: ActivatedRoute, public recetteService: RecetteService) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.recetteService.getRecette(+params.id).subscribe(
          (recette) => this.recette = recette
        );
      }
    );
  }

}
