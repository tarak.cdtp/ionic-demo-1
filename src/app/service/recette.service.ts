import {Injectable} from '@angular/core';
import {Recette} from '../model/recette';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecetteService {
  recettes: Recette[];

  constructor() {
    this.recettes = [
      {
        id: 1,
        nom: 'Couscous',
        image: 'https://img.cuisineaz.com/660x660/2016-04-28/i68325-couscous-et-sa-sauce.jpg',
        decription: 'Le couscous est servi le plus souvent avec un ragoût de légumes accompagné de viande, présenté parfois dans un plat en terre cuite traditionnel à tajine. Le plat de base consiste en l\'association du couscous (céréale) et d\'un apport protéinique, viande, poisson ou pois chiches (légumineuse jouant le rôle de l\'apport protéinique). Il peut aussi être consommé seul, aromatisé ou nature, chaud ou froid, comme dessert avec du sucre ou plat d\'accompagnement.',
        ingredients: [
          {
            name: 'couscous moyen',
            quantitee: '500 g'
          }, {
            name: 'poulet',
            quantitee: '1'
          }, {
            name: 'carottes',
            quantitee: '3'
          }
        ]
      },
      {
        id: 2,
        nom: 'Smoothies',
        image: 'https://img.cuisineaz.com/660x660/2020-05-25/i154095-smoothie-kiwi-green-mangue-coco-5628.jpeg',
        decription: 'Smoothie au kiwi vert Zespri ™ Green, mangue et coco',
        ingredients: [
          {
            name: 'kiwis verts Zespri',
            quantitee: '5'
          }, {
            name: 'càs de miel',
            quantitee: '2'
          }, {
            name: 'poignées de cubes de noix de coco',
            quantitee: '2'
          },
          , {
            name: 'lait de coco',
            quantitee: '75 cl'
          }
        ]
      }
    ];
  }

  getRecettes() {
    return of([...this.recettes]);
  }

  getRecette(id: number): Observable<Recette> {
    return of(this.recettes.find(r => r.id === id));
  }
}
